import { AuthConfig } from "../app/security/security.service";

export const environment = {
  production: true,
  search_url: "https://api.spotify.com/v1/search",

  auth_config: {
    url: "https://accounts.spotify.com/authorize",
    response_type: "token",
    redirect_uri: "http://localhost:4200/",
    client_id: "c73fbc5d49b54bc980767453e7eb9c14"
  } as AuthConfig
};
