interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists: Artist[];
  images: AlbumImage[];
}

export interface AlbumImage {
  url: string;
}

export interface Artist extends Entity {}

export interface PageObject<T> {
  items: T[];
  total: number;
  limit: number;
}

export interface AlbumsResponse {
  albums: PageObject<Album>;
}

export interface ArtistsResponse {
  artists: PageObject<Artist>;
}
