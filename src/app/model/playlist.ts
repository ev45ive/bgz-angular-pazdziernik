export interface Playlist {
  id: number;
  name: string;
  favourite: boolean;
  /**
   * Color in hex format
   */
  color: string;
  tracks?: Track[];
}

interface Track {
  id: number;
}
