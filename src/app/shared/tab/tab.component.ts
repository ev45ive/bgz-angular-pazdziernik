import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";

@Component({
  selector: "app-tab",
  templateUrl: "./tab.component.html",
  styleUrls: ["./tab.component.css"]
})
export class TabComponent implements OnInit {
  @Input()
  title: string;

  @Input()
  active: boolean;

  activeChange = new EventEmitter<boolean>();

  @Output()
  toggle() {
    this.activeChange.emit(true)
  }

  constructor() {}

  ngOnInit() {}
}
