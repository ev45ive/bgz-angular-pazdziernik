import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  ContentChild,
  ContentChildren,
  QueryList
} from "@angular/core";
import { TabComponent } from "../tab/tab.component";

@Component({
  selector: "app-tabs",
  templateUrl: "./tabs.component.html",
  styleUrls: ["./tabs.component.css"]
})
export class TabsComponent implements OnInit {
  @ViewChild("navigation")
  navigation: ElementRef;

  // @ContentChild('navigation')
  // navigation: ElementRef;

  @ContentChildren(TabComponent)
  tabs: QueryList<TabComponent>;

  active: TabComponent;

  constructor() {}

  updateTabs() {
    this.tabs.forEach(tab => {
      tab.active = this.active == tab;
    });
  }

  ngAfterContentInit() {
    // console.log(this.tabs);
    this.tabs.forEach(tab => {
      tab.activeChange.subscribe(() => {
        this.active = this.active !== tab ? tab : null;
        this.updateTabs();
      });
    });
  }

  ngOnInit() {}
}
