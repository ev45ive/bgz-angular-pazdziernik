import { BrowserModule } from "@angular/platform-browser";
import { NgModule, ApplicationRef } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { PlaylistsModule } from "./playlists/playlists.module";
import { SharedModule } from "./shared/shared.module";
import { MusicModule } from "./music/music.module";
import { SecurityModule } from "./security/security.module";
import { TestingComponent } from "./testing/testing.component";
import { FormsModule } from "@angular/forms";
import { environment } from "../environments/environment";

@NgModule({
  declarations: [AppComponent, TestingComponent],
  imports: [
    FormsModule,
    BrowserModule,
    PlaylistsModule,
    SharedModule,
    MusicModule,
    AppRoutingModule,
    SecurityModule.forRoot(environment.auth_config)
  ],
  // entryComponents: [AppComponent, TestingComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
