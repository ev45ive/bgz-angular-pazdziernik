import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { environment } from "../../environments/environment";
import { AuthConfig, SecurityService } from "./security.service";
import { AuthInterceptorService } from "src/app/security/auth-interceptor.service";
import { HTTP_INTERCEPTORS } from "@angular/common/http";

@NgModule({
  imports: [CommonModule],
  declarations: [],
  providers: [
    {
      provide: AuthConfig,
      useValue: environment.auth_config
    },
    {
      multi: true,
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService
    }
  ]
})
export class SecurityModule {
  constructor(private security: SecurityService) {
    security.getToken();
  }

  static forRoot(config: AuthConfig) {
    return <ModuleWithProviders>{
      ngModule: SecurityModule,
      providers: [
        {
          provide: AuthConfig,
          useValue: config
        }
      ]
    };
  }
}
