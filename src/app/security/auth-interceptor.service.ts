import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpResponse,
  HttpEvent,
  HttpErrorResponse
} from "@angular/common/http";
import { Observable, of, never, throwError } from "rxjs";
import { SecurityService } from "./security.service";
import { tap, catchError, retry, retryWhen } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private security: SecurityService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const reqWithAuth = req.clone({
      setHeaders: {
        Authorization: "Bearer " + this.security.getToken()
      }
    });

    return next.handle(reqWithAuth).pipe(
      catchError((error, caught) => {
        if (error instanceof HttpErrorResponse) {
          // ...
          if (error.status === 401) {
            this.security.authorize();
          }
        }

        return throwError(error.error.error)
        // return [];
      })
    );
  }
}
