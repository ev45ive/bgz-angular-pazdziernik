import { Injectable } from "@angular/core";
import { HttpParams } from "@angular/common/http";

export class AuthConfig {
  url: string;
  client_id: string;
  response_type: string;
  redirect_uri: string;
}

@Injectable({
  providedIn: "root"
})
export class SecurityService {
  constructor(private config: AuthConfig) {}

  authorize() {
    const { url, client_id, redirect_uri, response_type } = this.config;

    // const auth_url = `${url}?client_id=${client_id}&redirect_uri${redirect_uri}&response_type=${response_type}`;

    const params = new HttpParams({
      fromObject: {
        client_id,
        redirect_uri,
        response_type
      }
    });
    const auth_url = `${url}?${params.toString()}`;

    sessionStorage.removeItem("token");
    location.replace(auth_url);
  }

  token = "";

  getToken() {
    this.token = JSON.parse(sessionStorage.getItem("token"));

    if (!this.token && location.hash) {
      const params = new HttpParams({
        fromString: location.hash
      });
      this.token = params.get("#access_token");
      sessionStorage.setItem("token", JSON.stringify(this.token));
    }

    if (!this.token) {
      this.authorize();
    }

    return this.token;
  }
}
