import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PlaylistsViewComponent } from "./playlists-view/playlists-view.component";
import { ItemsListComponent } from "./items-list/items-list.component";
import { ListItemComponent } from "./list-item/list-item.component";
import { PlaylistDetailsComponent } from "./playlist-details/playlist-details.component";
import { FormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";
import { PlaylistsRoutingModule } from "./playlists-routing.module";
import { PlaylistContainerComponent } from './playlist-container/playlist-container.component';

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule, PlaylistsRoutingModule],
  declarations: [
    PlaylistsViewComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    PlaylistContainerComponent
  ],
  entryComponents: [PlaylistsViewComponent],
  exports: [PlaylistsViewComponent]
})
export class PlaylistsModule {}
