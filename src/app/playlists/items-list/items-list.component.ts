import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output
} from "@angular/core";
import { Playlist } from "../../model/playlist";
import { NgForOfContext } from "@angular/common";
NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.css"],
  encapsulation: ViewEncapsulation.Emulated
  // inputs:[
  //   'playlists:items'
  // ]
})
export class ItemsListComponent implements OnInit {

  hover:Playlist = null

  @Input("items")
  playlists: Playlist[] = [];

  @Input()
  selected: Playlist = null;

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  select(playlist: Playlist) {
    this.selectedChange.emit(playlist);
  }

  trackFn(index: number, item: Playlist) {
    return item.id;
  }

  constructor() {}

  ngOnInit() {}
}
