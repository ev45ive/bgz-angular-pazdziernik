import { Component, OnInit } from "@angular/core";
import { PlaylistsService } from "../playlists.service";
import { Playlist } from "src/app/model/playlist";
import { ActivatedRoute } from "@angular/router";
import { map, switchMap, tap } from "rxjs/operators";

@Component({
  selector: "app-playlist-container",
  templateUrl: "./playlist-container.component.html",
  styleUrls: ["./playlist-container.component.css"]
})
export class PlaylistContainerComponent implements OnInit {
  selected$ = this.route.paramMap.pipe(
    map(paramMap => paramMap.get("id")),
    // tap(id => this.service.select(parseInt(id))),
    switchMap(id => this.service.getPlaylist(parseInt(id)))
  );

  update(playlist: Playlist) {
    this.service.update(playlist);
  }

  constructor(
    private route: ActivatedRoute,
    private service: PlaylistsService
  ) {}

  ngOnInit() {}
}
