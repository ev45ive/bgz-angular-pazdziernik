import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { PlaylistsViewComponent } from "./playlists-view/playlists-view.component";
import { PlaylistContainerComponent } from "./playlist-container/playlist-container.component";

const routes: Routes = [
  {
    path: "playlists",
    component: PlaylistsViewComponent
  },
  {
    path: "playlists/:id",
    component: PlaylistsViewComponent,
    children: [
      {
        path: "",
        component: PlaylistContainerComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistsRoutingModule {}
