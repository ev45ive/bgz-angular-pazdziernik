import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Playlist } from "../../model/playlist";
import { NgForm } from "@angular/forms";
// import { NgIf } from "@angular/common";
// NgIf

@Component({
  selector: "app-playlist-details",
  templateUrl: "./playlist-details.component.html",
  styleUrls: ["./playlist-details.component.css"]
})
export class PlaylistDetailsComponent implements OnInit {
  @Input()
  playlist: Playlist = {
    id: 123,
    name: "Angular Hits",
    color: "#ff0000",
    favourite: true
  };

  mode = "show";

  constructor() {}

  ngOnInit() {}

  edit() {
    this.mode = "edit";
  }

  cancel() {
    this.mode = "show";
  }

  @Output()
  playlistChange = new EventEmitter<Playlist>();

  save(modelRef: NgForm) {
    const playlist: Playlist = {
      ...this.playlist,
      ...modelRef.value
    };
    this.playlistChange.emit(playlist);
  }
}
