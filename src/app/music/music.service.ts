import { Injectable, Inject } from "@angular/core";
import { Album } from "src/app/model/Album";
import { HttpClient } from "@angular/common/http";
import { AlbumsResponse } from "../model/Album";
import { BehaviorSubject } from "rxjs";
import { map, switchMap } from "rxjs/operators";

@Injectable({
  providedIn: "root" // provided in app module
})
export class MusicService {
  albums$ = new BehaviorSubject<Album[]>([]);
  query$ = new BehaviorSubject<string>("batman");

  constructor(
    @Inject("SEARCH_URL") private search_url: string,
    private http: HttpClient
  ) {
    this.query$
      .pipe(
        switchMap(query =>
          this.http.get<AlbumsResponse>(this.search_url, {
            params: {
              type: "album",
              q: query
            }
          })
        ),
        map(response => response.albums.items)
      )
      .subscribe(
        albums => {
          this.albums$.next(albums);
        },
        // error => this.errors$.next(error)
      );
  }

  search(query = "batman") {
    this.query$.next(query);
  }

  getQuery() {
    return this.query$.asObservable();
  }

  getAlbums(query = "batman") {
    return this.albums$.asObservable();
  }
}
