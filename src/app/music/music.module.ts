import { NgModule, Inject } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MusicSearchComponent } from "./music-search/music-search.component";
import { SearchFormComponent } from "./search-form/search-form.component";
import { AlbumsGridComponent } from "./albums-grid/albums-grid.component";
import { AlbumCardComponent } from "./album-card/album-card.component";
import { environment } from "../../environments/environment";
import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from "@angular/forms";
// import { MusicService } from "src/app/music/music.service";

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  declarations: [
    MusicSearchComponent,
    SearchFormComponent,
    AlbumsGridComponent,
    AlbumCardComponent
  ],
  providers: [
    {
      provide: "SEARCH_URL",
      useValue: environment.search_url
    }
    // {
    //   provide: MusicService,
    //   useFactory: (url: string) => {
    //     return new MusicService(url);
    //   },
    //   deps: ["SEARCH_URL"]
    // },
    // {
    //   provide: AbstractMusicService,
    //   useClass: SpotifyMusicService
    // },
    // {
    //   provide: MusicService,
    //   useClass: MusicService
    // }
    // MusicService,
  ],
  exports: [MusicSearchComponent]
})
export class MusicModule {}
