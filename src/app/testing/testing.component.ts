import { Component, OnInit } from "@angular/core";
import { TestingService } from "./testing.service";

@Component({
  selector: "app-testing",
  templateUrl: "./testing.component.html",
  styleUrls: ["./testing.component.css"]
})
export class TestingComponent implements OnInit {
  data: {
    name: string;
  }[];
  message = "message";

  constructor(private service: TestingService) {}

  fetchData(query: string) {
    this.service.fetchData(query).subscribe(data => (this.data = data));
  }

  ngOnInit() {}
}
