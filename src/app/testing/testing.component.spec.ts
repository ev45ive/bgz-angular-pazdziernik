import { TestingComponent } from "./testing.component";
import {
  TestBed,
  ComponentFixture,
  async,
  inject
} from "@angular/core/testing";
import { By } from "@angular/platform-browser";
import { FormsModule } from "@angular/forms";
import { TestingService } from "./testing.service";
import { of, Subject } from "rxjs";
import { EventEmitter } from "@angular/core";

fdescribe("Testing component", () => {
  let fixture: ComponentFixture<TestingComponent>;
  let instance: TestingComponent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [TestingComponent],
      providers: [
        {
          provide: TestingService,
          useClass: TestingService
        }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    instance = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(instance).toBeDefined();
  });

  it("should render message", () => {
    instance.message = "Lubie placki!";
    fixture.detectChanges();
    const elem = fixture.debugElement.query(By.css(".message"));

    expect(elem.nativeElement.innerText).toMatch("Lubie placki!");
  });

  it("should show message inside input", async(() => {
    const input = fixture.debugElement.query(By.css("input"));

    fixture.whenRenderingDone().then(() => {
      expect(input.nativeElement.value).toEqual(instance.message);
    });
  }));

  it("should update message when input value changes", () => {
    const inputElem = fixture.debugElement.query(By.css("input"));

    inputElem.nativeElement.value = "placki";
    inputElem.nativeElement.dispatchEvent(new Event("input"));

    // Albo:

    inputElem.triggerEventHandler("input", {
      // $event
      target: {
        value: "placki"
      }
    });

    expect(instance.message).toBe("placki");
  });

  it("should fetch data from service", inject(
    [TestingService],
    (service: TestingService) => {
      // TestBed.get(TestingService)
      const fakeData = [{ name: "Alice" }];
      const fakeObservable = new EventEmitter();

      const mockFn = spyOn(service, "fetchData").and.returnValue(
        fakeObservable
      );

      instance.fetchData("batman");
      expect(mockFn).toHaveBeenCalledWith("batman");

      fakeObservable.emit(fakeData);
      expect(instance.data).toEqual(fakeData);
    }
  ));
});
